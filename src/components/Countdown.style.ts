import { createStyles, makeStyles } from "@material-ui/core/styles";

export default makeStyles((theme) => createStyles({
    digit: {
        backgroundColor: 'rgba(200, 200, 200, 0.3)',
        margin: 5,
        padding: 5,
        width: 30,
        display: 'inline-block',
        borderRadius: 5,
        fontSize: 24,
    },
    hours: {
        '&::after': {
            content: '":"',
            fontSize: 24,
            margin: 10
        }
    },
    minutes: {
        '&::after': {
            content: '":"',
            fontSize: 24,
            margin: 10
        }
    },
    seconds: {},
    countdown: {}
}),
);
