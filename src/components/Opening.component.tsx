import { h, FunctionalComponent } from 'preact';
import Opening from '../classes/Opening.class';
import useStyle from './Opening.style'

interface OpeningProps {
    opening: Opening;
}

const OpeningComponent: FunctionalComponent<OpeningProps> = props => {

    const classes = useStyle();

    let dateOpen = new Date(props.opening.open.getTime() - 900000)
    let dateClose = new Date(props.opening.close.getTime() + 900000)

    let minutesOpen = dateOpen.getMinutes()
    let minutesClose = dateClose.getMinutes()
    let hoursOpen = dateOpen.getHours()
    let hoursClose = dateClose.getHours()

    return (
        <div className={classes.item}>
            <h3>{hoursOpen < 12 ? "Matin" : "Après-midi"}</h3>

            {
                hoursOpen === hoursClose &&
                minutesOpen === minutesClose ?

                <p>Le pont sera fermé si des bateaux se présentent pour entrer ou sortir du port à {hoursOpen}h{minutesOpen < 10 ? "0" + minutesOpen : minutesOpen}.</p>
                :
                <p>
                    Fermeture : {hoursOpen}h{minutesOpen < 10 ? "0" + minutesOpen : minutesOpen} <br/>
                    Ouverture : {hoursClose}h{minutesClose < 10 ? "0" + minutesClose : minutesClose}
                </p>
            }
        </div>
    );
}
  
export default OpeningComponent;