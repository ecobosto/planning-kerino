import React from 'react';
import './App.css';
import { AppBar, Toolbar, Avatar, Typography, Grid, IconButton, Popover, Card, CardContent } from '@material-ui/core';
import { ThemeProvider, Theme, createStyles, makeStyles } from '@material-ui/core';
import HelpIcon from '@material-ui/icons/Help';
import * as Schedule from './Schedule';
import * as MainCard from "./MainCard";
import th from "./Theme"

import logo from "./assets/logo_a.png";

let WrapperScheduleCard = Schedule.default.WrapperScheduleCard;
let WrapperMainCard = MainCard.default.WrapperMainCard;

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    app: {
    },
    appBar: {
      marginBottom: 20,
    },
    toolbar: {
      minHeight: 70,
      justifyContent: "center",
    },
    large: {
      width: 60,
      height: 60,
    },
    help: {
      marginLeft: 40
    },
    helpCard: {
      minWidth : 260,
      maxWidth : 260,
      borderRadius : 10
    }
  }),
);



function App() {
  const classes = useStyles(th);

  const [anchorEl, setAnchorEl] = React.useState<HTMLButtonElement | null>(null);

  const displayHelpCard = (event: React.MouseEvent<HTMLButtonElement>) => {
    setAnchorEl(event.currentTarget);
  }

  const handleClose = () => {
    setAnchorEl(null);
  };

  var isShowingHelp = Boolean(anchorEl);

  var topV = 200;
  var widthV = window.innerWidth/2 - 10;

  return (
    <div className={classes.app}>
      <ThemeProvider theme={th}>
        <AppBar position="static" className={classes.appBar} color='primary'>
          <Toolbar className={classes.toolbar}>
            <Avatar alt="Ecobosto x Kérino" src={logo} className={classes.large} />
            <Typography variant="h6">Planning Kérino</Typography>
            <div className={classes.help}>
              <IconButton onClick={displayHelpCard} aria-label="delete" edge="end">
                <HelpIcon color="secondary" />
              </IconButton>
            </div>
            <Popover
              open={isShowingHelp}
              anchorEl={anchorEl}
              onClose={handleClose}
              anchorReference="anchorPosition"
              anchorPosition={{ top: topV, left: widthV }}
              anchorOrigin={{
                vertical: 'bottom',
                horizontal: 'left',
              }}
              transformOrigin={{
                vertical: 'center',
                horizontal: 'center',
              }}
            >
              <Card className={classes.helpCard}>
                <CardContent>
                  <Typography gutterBottom variant="h5" component="h2">
                    Aide
                  </Typography>
                  <Typography variant="body2" component="p" align="justify">
                    <span style={{ color: "#134e5e", fontWeight: "bold" }} >Fermeture</span>
                    : Horaire correspondant à l'heure à laquelle le pont s'ouvre pour les véhicules maritimes et n'est donc plus accessible aux piétons.
                  </Typography>
                  <br />
                  <Typography variant="body2" component="p" align="justify">
                    <span style={{ color: "#134e5e", fontWeight: "bold" }} >Ouverture</span>
                    : Horaire correspondant à l'heure à laquelle le pont se ferme pour les véhicules maritimes et donc est accessible aux piétons.
                  </Typography>
                  <br />
                  <Typography variant="body2" component="p" align="justify">
                    © 2021 Planning Kérino développé par Paul pour Ecobosto™
                  </Typography>
                </CardContent>
              </Card>
            </Popover>
          </Toolbar>
        </AppBar>

        <Grid container spacing={4} direction="column" alignItems="center">

          <WrapperMainCard />
          <WrapperScheduleCard />

        </Grid>
      </ThemeProvider>
    </div>
  );
}

export default App;
