import Opening from '../classes/Opening.class';
import OpeningComponent from './Opening.component';
import useStyle from './List.style';
import { h, FunctionalComponent } from 'preact';
import { useEffect, useState } from 'preact/hooks';

interface ListProps {
    list: Opening[];
    day: Date;
    title: boolean
}

const ListComponent: FunctionalComponent<ListProps> = props => {

    const classes = useStyle();

    const [data, setData] = useState<Opening[]>([]);

    useEffect(() => {
        let tmpList: Opening[] = [];
        props.list.forEach((value) => {
            if (value.date.getDate() === props.day.getDate() &&
                value.date.getMonth() === props.day.getMonth())
                    tmpList.push(value)
        })
        setData(tmpList)
    }, [props])

    const capitalizeFirstLetter = (string: string) => {
        return string.charAt(0).toUpperCase() + string.slice(1);
    }

    let options = { weekday: 'long', month: 'long', day: 'numeric' };

    let title = capitalizeFirstLetter(
        props.day.toLocaleDateString(
            'fr-FR', 
            options as Intl.DateTimeFormatOptions
        )
    )

    return (
        <div className={classes.list}>
            {
                props.title &&
                <h1 className={classes.title}>{title}</h1>
            }
            {
                data.map(elem => {
                    return (
                        <OpeningComponent opening={elem} />
                    )
                })
            }
        </div>
    );
}
  
export default ListComponent;