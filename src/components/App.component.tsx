import ThemeProvider from '@material-ui/styles/ThemeProvider';
import InterfaceComponent from './Interface.component';
import { FunctionalComponent, h } from 'preact';
// import Theme from '../Theme';
import { createTheme } from "@material-ui/core/styles"
import useMediaQuery from '@material-ui/core/useMediaQuery';
import { useMemo } from 'preact/hooks';
/**
 * Composant de base de l'application. Initialise Material-UI avec le thème custom
 * @returns l'application preact
 */
const App: FunctionalComponent = () => {

    const prefersDarkMode = useMediaQuery('(prefers-color-scheme: dark)');

    const theme = useMemo(
        () =>
          createTheme({
            palette: {
              type: prefersDarkMode ? 'dark' : 'light',
              primary: {
                main: "#006966",
                light: prefersDarkMode ? '#353535' : '#ffffff',
                dark: prefersDarkMode ? '#cccccc' : '#000000',
              }
            },
          }),
        [prefersDarkMode],
      );
    return (
        <div id="preact_root">
            <ThemeProvider theme={theme}>
                <InterfaceComponent />
            </ThemeProvider>
        </div>
    );
};

export default App;
