import Papa from "papaparse";
import Opening from "../classes/Opening.class";

const URL = "/assets/port-vannes.csv";

const Parser = (callback: (results: Opening[]) => void) => {
    Papa.parse<Opening>(URL, {
        download: true,
        header: true,
        dynamicTyping: true,
        complete: (result) => {
            callback(Processing(result))
        },
    });
}

const Processing = (parseResults: Papa.ParseResult<Opening>) => {
    let finalResults: Opening[] = [];

    for (let result of parseResults.data) {
        let close = new Date(result.date + "T" + result.close)
        if (new Date() <= close) {
            finalResults.push(new Opening({
                'date': new Date(result.date),
                'open': result.open && new Date(result.date + "T" + result.open),
                'close': close
            }))
        }
    }

    finalResults.sort((a, b) => a.open > b.open ? 1 : -1)

    return finalResults;
}


export default Parser;