import { createStyles, makeStyles } from "@material-ui/core/styles"

export default makeStyles((theme) => createStyles({
    
    rounded: {
        borderRadius: '0 0 50% 50%',
        display: 'block',
        background: theme.palette.primary.light,
        width: '100%',
        height: 70,
        marginTop: -35,
        [theme.breakpoints.down('md')]: {
            height: 30,
            marginTop: -15
        },

        '&:after': {
            content: "",
            display: 'block',
            width: 'inherit',
            height: 35,
            background: theme.palette.primary.light,
            [theme.breakpoints.down('md')]: {
                height: 15
            }
        }
    },
    mainHeader: {
        display: 'flex',
        justifyContent: 'space-between',
        padding: 15,
    },
    header: {
        background: theme.palette.primary.light,
        color: theme.palette.primary.dark,
    },
    title :{
        margin: 0
    },
    popover: {
        padding: '0 15px',
        margin: '0 15px',
        maxWidth: 300,
    },
    emphasize: {
        color: '#006966',
        fontWeight: 'bold',
    },
    subheader: {
        display: 'flex',
        marginBottom: 15,
        marginRight: 0
    },
    chipMonth: {
        backgroundColor: '#006966',
        borderRadius: '0 15px 15px 0',
        padding: 15,
        fontSize: 20,
        color: 'white',

        [theme.breakpoints.down('sm')]: {
            display: 'none'
        }
    },
    chipDay: {
        backgroundColor: '#EFEFEF',
        borderRadius: 15,
        marginLeft: 10,
        color: '#505050',
        textAlign: 'center',
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'center',
        width: 54,

        '&.Mui-selected' : {
            borderRadius: '15px 15px 0 0',
        }
    },
    dayNumber : {
        fontSize: 16,
    },
    helpIcon:  {
        paddingRight: 15,
    },
    tabsContainer: {
        display: 'inline',
        overflow: 'hidden',
        alignSelf: 'center'
    },
    chipTextWrap: {
        lineBreak: 'strict'
    }
}),
)
