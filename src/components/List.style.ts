import { createStyles, makeStyles } from "@material-ui/core/styles";

export default makeStyles((theme) => createStyles({
    
    list : {
        paddingTop: 50,
        textAlign: 'center',
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
    },
    title: {
      color: theme.palette.primary.light,
    }
}),
);
