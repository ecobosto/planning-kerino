import React, { Component, PureComponent } from "react";
import { Card, CardContent, Chip, CircularProgress, Grid, Typography } from "@material-ui/core";
import * as Papa from "papaparse";

//var CSVURL = "https://cors-anywhere.herokuapp.com/https://ecobosto.fr/assets/port-vannes.txt"
var CSVURL = "/port-vannes.csv";
var SCHEDULE: Array<any> = [];

class WrapperScheduleCard extends PureComponent<{}, { isLoaded: boolean, data: any }> {
  constructor(props: any) {
    super(props);

    this.state = {
      isLoaded: false,
      data: null
    };
  }

  componentDidMount() {
    var obj = this
    Papa.parse(CSVURL, {
      download:true,
      complete: async function (results) {
        await dataProcessing(results);
        obj.setState({
          isLoaded: true,
          data: SCHEDULE
        });
      },
    });
  }

  render() {
    const { isLoaded, data } = this.state;

    return <>{
      isLoaded ? <ScheduleCard data={data} /> : <CircularProgress color="secondary" />
    }</>
  }
}

class ScheduleCard extends Component<{ data: any }, {}> {
  render() {
    return <> {
      SCHEDULE.map((el: any) => {
        var date = new Date()

        if (el.day >= date.getDate() && getMonthNumber(el.month) >= date.getMonth() + 1) {
          var day = ""
          if (el.day == date.getDate() && getMonthNumber(el.month) == date.getMonth() + 1) {
            day = "Aujourd'hui";
          } else {
            day = `${el.day_name} ${el.day} ${el.month}`;
          }

          let morning = null
          let afternoon = null
          let day_chip = null

          if (el.morning != null) {
            morning = <Grid item xs>
              <Card /*className={classes.scheduleCard}*/ className="scheduleCard">
                <CardContent>
                  <Typography align="left" color="primary">
                    Matin
                  </Typography>
                  <Typography>
                    Fermeture : {el.morning?.opening.hour}h{el.morning?.opening.minute}
                  </Typography>
                  <Typography>
                    Ouverture : {el.morning?.closure.hour}h{el.morning?.closure.minute}
                  </Typography>
                </CardContent>
              </Card>
            </Grid>
          }

          if (el.afternoon != null) {
            afternoon = <Grid item xs>
              <Card /*className={classes.scheduleCard}*/ className="scheduleCard">
                <CardContent>
                  <Typography align="left" color="primary">
                    Après-midi
                  </Typography>
                  <Typography>
                    Fermeture : {el.afternoon?.opening.hour}h{el.afternoon?.opening.minute}
                  </Typography>
                  <Typography>
                    Ouverture : {el.afternoon?.closure.hour}h{el.afternoon?.closure.minute}
                  </Typography>
                </CardContent>
              </Card>
            </Grid>
          }

          if (morning != null || afternoon != null) {
            day_chip = <Grid item xs container justify="flex-start">
              <Chip /*className={classes.chipDay}*/ className="chipDay" label={day} color="primary" />
            </Grid>
          }

          return <Grid key={el.id} item xs={12}>
            <Grid container direction="column" spacing={2}>
              {day_chip}
              {morning}
              {afternoon}
            </Grid>
          </Grid>
        }
        return <div key={el.id}></div>
      })
    }</>
  }
}

async function dataProcessing(parsedData: Papa.ParseResult<any>) {
  let count: number = 0;

  parsedData.data.forEach((lilArray) => {
    if (lilArray.length !== 0) {
      let dayObj = {
        id: count,
        day_name: "",
        day: 0,
        month: "",
        year: 0,
        morning: {},
        afternoon: {},
      };

      let dateSplit = lilArray[0].split("/");

      let d = new Date(parseInt(dateSplit[2]), parseInt(dateSplit[1]) - 1, parseInt(dateSplit[0]))

      dayObj.day = parseInt(dateSplit[0]);
      dayObj.day_name = getDayName(d.getDay())
      dayObj.month = getMonthName(parseInt(dateSplit[1]))
      dayObj.year = parseInt(dateSplit[2]);

      if (lilArray.length == 3) {
        let op1 = lilArray[1].split(":")
        let cl1 = lilArray[2].split(":")

        if (parseInt(op1[0]) >= 12) {
          dayObj.afternoon = { opening: { hour: op1[0], minute: op1[1] }, closure: { hour: cl1[0], minute: cl1[1] } }
          dayObj.morning = null as any
        } else {
          dayObj.morning = { opening: { hour: op1[0], minute: op1[1] }, closure: { hour: cl1[0], minute: cl1[1] } }
          dayObj.afternoon = null as any
        }
      } else if (lilArray.length == 5) {
        let op1 = lilArray[1].split(":")
        let cl1 = lilArray[2].split(":")

        let op2 = lilArray[3].split(":")
        let cl2 = lilArray[4].split(":")

        dayObj.morning = { opening: { hour: op1[0], minute: op1[1] }, closure: { hour: cl1[0], minute: cl1[1] } }
        dayObj.afternoon = { opening: { hour: op2[0], minute: op2[1] }, closure: { hour: cl2[0], minute: cl2[1] } }
      } else {
        dayObj.morning = null as any
        dayObj.afternoon = null as any
      }

      SCHEDULE.push(dayObj);
      count++;
    }
  });
}

function getDayName(dayNumber: number) {
  switch (dayNumber) {
    case 1:
      return "Lundi"
    case 2:
      return "Mardi"
    case 3:
      return "Mercredi"
    case 4:
      return "Jeudi"
    case 5:
      return "Vendredi"
    case 6:
      return "Samedi"
    case 0:
      return "Dimanche"
    default:
      return "NODAY"
  }
}

function getMonthNumber(monthName: string) {
  switch (monthName.toUpperCase()) {
    case "JANVIER":
      return 1
    case "FEVRIER":
      return 2
    case "MARS":
      return 3
    case "AVRIL":
      return 4
    case "MAI":
      return 5
    case "JUIN":
      return 6
    case "JUILLET":
      return 7
    case "AOUT":
      return 8
    case "SEPTEMBRE":
      return 9
    case "OCTOBRE":
      return 10
    case "NOVEMBRE":
      return 11
    case "DECEMBRE":
      return 12
    default:
      return 0
  }
}

function getMonthName(monthNumber: number) {
  switch (monthNumber) {
    case 1:
      return "Janvier"
    case 2:
      return "Février"
    case 3:
      return "Mars"
    case 4:
      return "Avril"
    case 5:
      return "Mai"
    case 6:
      return "Juin"
    case 7:
      return "Juillet"
    case 8:
      return "Août"
    case 9:
      return "Septembre"
    case 10:
      return "Octobre"
    case 11:
      return "Novembre"
    case 12:
      return "Décembre"
    default:
      return "None"
  }
}

let exported = {
  WrapperScheduleCard,
  getMonthNumber
}

export default exported