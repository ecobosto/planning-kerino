import * as Papa from "papaparse";

var schedule: any = undefined;

var url: string = "/port-vannes.csv";

function getSchedule() : Array<any>{
  Papa.parse(url, {
    download: true,
    complete: async function (results) {
      await dataProcessing(results);
    },
  })
  return schedule
}

function dataProcessing(parsedData: Papa.ParseResult<any>) {
  let ret: Array<any> = [];

  let count: number = 0;
  parsedData.data.forEach((lilArray) => {
    if (lilArray.length !== 0) {
      let dayObj = {
        id: count,
        day_name: "",
        day: 0,
        month: "",
        year: 0,
        morning: null,
        afternoon: null,
      };

      ret.push(dayObj);
      count++;
    }
  });
  schedule = ret;
}

/*var schedule = [
    {
      id: 1,
      day_name: "Lundi",
      day: 3,
      month: "Juillet",
      year: 2021,
      morning: null,
      afternoon: {
        opening: {
          hour: "15",
          minute: "00",
        },
        closure: {
          hour: "18",
          minute: "00",
        }
      }
    },
    {
      id: 2,
      day_name: "Vendredi",
      day: 1,
      month: "Juillet",
      year: 2021,
      morning: {
        opening: {
          hour: "8",
          minute: "00",
        },
        closure: {
          hour: "11",
          minute: "00",
        }
      },
      afternoon: {
        opening: {
          hour: "19",
          minute: "00",
        },
        closure: {
          hour: "19",
          minute: "30",
        }
      }
    },
    {
      id: 3,
      day_name: "Mercredi",
      day: 30,
      month: "Juin",
      year: 2021,
      morning: {
        opening: {
          hour: "8",
          minute: "00",
        },
        closure: {
          hour: "10",
          minute: "30",
        }
      },
      afternoon: {
        opening: {
          hour: "21",
          minute: "00",
        },
        closure: {
          hour: "22",
          minute: "00",
        }
      }
    }
  ]*/

let eo = {
  getSchedule,
  schedule
}
export default eo;
//export default schedule
