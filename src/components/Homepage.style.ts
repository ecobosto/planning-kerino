import { createStyles, makeStyles } from "@material-ui/core/styles";

export default makeStyles((theme) => createStyles({
    
    message: {
        paddingTop: 50,
        textAlign: 'center',
        color: theme.palette.primary.light,

        '& h1' : {
            fontSize: '3rem'
        },
        '& p' : {
            fontSize: '1.5rem'

        }
    }
}),
);
