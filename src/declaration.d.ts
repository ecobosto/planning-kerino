/**
 * Déclaration du type CSS pour pouvoir l'utiliser en TypeScript
 */
declare module "*.css" {
    const mapping: Record<string, string>;
    export default mapping;
}

/**
 * Déclaration du type CSV pour pouvoir l'utiliser en TypeScript
 */
declare module "*.csv" {
    const content: string;
    export default content;
}
