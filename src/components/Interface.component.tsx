import IconButton from '@material-ui/core/IconButton';
import Popover from '@material-ui/core/Popover';
import Tabs from '@material-ui/core/Tabs';
import HelpIcon from '@material-ui/icons/Help';
import Tab from '@material-ui/core/Tab';
import Opening from '../classes/Opening.class';
import Parser from '../parsing/Parser';
import ListComponent from './List.component';
import HomepageComponent from './Homepage.component';
import useStyle from './Interface.style'
import { FunctionalComponent, h } from 'preact';
import { useEffect, useState } from 'preact/hooks';
import SwipeableViews from 'react-swipeable-views';
import CircularProgress from '@material-ui/core/CircularProgress';

const InterfaceComponent: FunctionalComponent = () => {

    const classes = useStyle();

    const [anchorEl, setAnchorEl] = useState<HTMLButtonElement | null>(null);
    const [chips, setChips] = useState<JSX.Element[]>([]);
    const [views, setViews] = useState<JSX.Element[]>([]);
    const [data, setData] = useState<Opening[]>([]);
    const [index, setIndex] = useState<number>(0);

    useEffect(() => {
        Parser((results: Opening[]) => { setData(results) });
        generateChips();
    }, [])

    useEffect(() => {
        generateViews();
    }, [data])

    /**
     * Les chips sont les éléments dans le header pour choisir le jour
     * Au chargement on génère la liste pour les 10 prochains jours
     */
    const generateChips = () => {
        let tmpChips = [];
        let tmpDate = new Date();

        for (let i = 0; i < 10; i++) {
            tmpChips.push(
                <Tab label={
                    <div className={classes.chipTextWrap}>
                        <span className={classes.chipTextWrap}>{tmpDate.toLocaleString('fr-FR', { weekday: 'short' }).toLocaleUpperCase() }</span>{tmpDate.getDate()}
                    </div>
                } value={i} key={i} className={classes.chipDay} />
            )
            tmpDate.setDate(tmpDate.getDate() + 1)
        }

        setChips(tmpChips)
    }

    /**
     * Lorsque le fichier CSV est chargé, on génère les vues 
     * C'est le contenu affiché pour le jour sélectionné
     */
    const generateViews = () => {
        let now = new Date()
        let tmpViews = [];
        let tmpDate = new Date();

        for (let i = 0; i < 10; i++) {

            if (now.getMonth() === tmpDate.getMonth() && now.getDate() === tmpDate.getDate())
                tmpViews.push(
                    <div key={i}>
                        <HomepageComponent item={data && data[0]} />
                        <ListComponent title={false} list={data} day={new Date(tmpDate.getTime())} />
                        <p>Si les horaires ne s'affichent pas, vous pouvez consulter <a href="https://www.passerelle-vannes.fr/">ce site</a>.</p>
                    </div>
                )
            else {
                // Afficher les données pour le jour donné
                tmpViews.push(<ListComponent title={true} list={data} day={new Date(tmpDate.getTime())} key={i} />) 
            }

            tmpDate.setDate(tmpDate.getDate() + 1)
        }

        setViews(tmpViews)

    }

    return (
        <div>
            <div className={classes.header}>
                <div className={classes.mainHeader}>
                    <h1 className={classes.title}>Planning Kerino</h1>
                    <div className={classes.helpIcon}>
                        <IconButton
                            onClick={(e) => setAnchorEl(e.currentTarget)}
                            aria-label="aide" edge="end">
                            <HelpIcon color="primary" />
                        </IconButton>
                        <Popover open={Boolean(anchorEl)}
                            anchorEl={anchorEl}
                            onClose={() => setAnchorEl(null)}>
                            <div className={classes.popover}>
                                <h2>Aide</h2>
                                <p>Cette application s'appuie sur les horaires théoriques publiés par l'exploitant du Port de Vannes, sous réserve de modifications.</p>
                                <p><span className={classes.emphasize} >Fermeture</span>
                                    : Horaire correspondant à l'heure à laquelle le pont s'ouvre pour les véhicules maritimes et n'est donc plus accessible aux piétons.</p>
                                <p><span className={classes.emphasize} >Ouverture</span>
                                    : Horaire correspondant à l'heure à laquelle le pont se ferme pour les véhicules maritimes et donc est accessible aux piétons.</p>
                                <p>© 2021 Planning Kérino développé par Paul, Matisse et Gildas pour Ecobosto</p>
                            </div>
                        </Popover>
                    </div>
                </div>
                <div className={classes.subheader}>
                    <div className={classes.chipMonth}>{new Date().toLocaleString('fr-Fr', { month: 'short' }).toLocaleUpperCase()}</div>
                    
                    <div className={classes.tabsContainer}>
                        
                        <Tabs
                            value={index}
                            variant="scrollable"
                            onChange={(e, val) => {
                                setIndex(val);
                            }}
                            indicatorColor="primary"
                        >
                            {chips}
                        </Tabs>
                        
                    </div>
                </div>
            </div>

            <div className={classes.rounded}></div>

            {
                data ?
                    <SwipeableViews index={index} enableMouseEvents children={views} onChangeIndex={(i) => {setIndex(i)}} />
                :
                    <CircularProgress />
            }

        </div>
    )
}

export default InterfaceComponent;