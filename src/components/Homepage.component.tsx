import Opening from '../classes/Opening.class';
import CountdownComponent from './Countdown.component';
import useStyle from './Homepage.style';
import { Fragment, FunctionalComponent, h } from 'preact';
import { useEffect, useState } from 'preact/hooks';

interface HomepageProps {
    item: Opening;
}

const HomepageComponent: FunctionalComponent<HomepageProps> = (props) => {

    const classes = useStyle();

    const [status, setStatus] = useState<number>(0);
    const [item, setItem] = useState<Opening | null>(null);

    useEffect(() => {
        if (props.item) {
            setItem(props.item);

            if (Date.now() < props.item.open.getTime() - 900000) setStatus(0) // OPENED

            else if (Date.now() < props.item.open.getTime()) setStatus(1) // WILL CLOSE

            else if (Date.now() >= props.item.open.getTime() && 
                Date.now() < props.item.close.getTime()) setStatus(2) // CLOSED

            else if (Date.now() < props.item.close.getTime() + 900000) setStatus(3) // WILL OPEN

            else setStatus(4) // OPENED
        } else setStatus(5) // NOT LOADED
    }, [props])

    if (!item) {
        return (
            <div></div>
        )
    }

    switch(status) {
        case 5:
            return (
                <div></div>
            )
        case 0:
            return (
                <div className={classes.message}>
                    <h1>Le pont est ouvert</h1>
                    <p>Prochaine fermeture dans...</p>
                    <CountdownComponent date={item.open.getTime() - 900000} />
                </div>
            )
        case 1:
            return (
                <div className={classes.message}>
                    <h1>Le pont est en cours de fermeture</h1>
                    <p>Dépêchez vous ou prenez le tunnel.</p>
                </div>
            )
        case 2:
            return (
                <div className={classes.message}>
                    <h1>Le pont est fermé</h1>
                    <p>Prenez le tunnel de Kérino. Réouverture dans...</p>
                    <CountdownComponent date={item.close.getTime() + 900000} />
                </div>
            )
        case 3:
            return (
                <div className={classes.message}>
                    <h1>Le pont va bientôt rouvrir</h1>
                    <p>Patientez encore quelques minutes.</p>
                </div>
            )
        case 4:
            return (
                <div className={classes.message}>
                    <h1>Le pont est ouvert</h1>
                    <p>Profitez-en !</p>
                </div>
            )
        default:
            return <Fragment/>
    }

}
  
export default HomepageComponent;