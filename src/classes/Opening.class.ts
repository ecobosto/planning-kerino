export default class Opening {

    public date: Date;

    public open: Date;

    public close: Date;

    public constructor(row?: any) {
        this.date = row.date;
        this.open = row.open;
        this.close = row.close;
    }
}