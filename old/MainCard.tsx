import React, { Component, PureComponent } from "react";
import * as Papa from "papaparse";
import { Grid, Card, CardContent, Typography, CircularProgress } from "@material-ui/core";

//var CSVURL = "https://cors-anywhere.herokuapp.com/https://ecobosto.fr/assets/port-vannes.txt"
var CSVURL = "/port-vannes.csv";
var SCHEDULE: Array<any> = [];
var OPEN_CODE = 0;

var TITLE: string = "";
var PARA: string = "";
var CARD: any = null;

class WrapperMainCard extends PureComponent<{}, { isLoaded: boolean, data: any }> {
    constructor(props: any) {
        super(props);

        this.state = {
            isLoaded: false,
            data: null
        };
    }

    componentDidMount() {
        var obj = this
        Papa.parse(CSVURL, {
            download: true,
            complete: async function (results) {
                await dataProcessing(results);
                await isBridgeOpen()
                await setAllText()
                obj.setState({
                    isLoaded: true,
                    data: SCHEDULE,
                });
            },
        });
    }

    render() {
        const { isLoaded, data } = this.state;

        return <>{
            isLoaded ? <MainCard data={data} /> : <CircularProgress color="primary" />
        }</>
    }
}

class MainCard extends Component<{ data: any }, {}> {
    render() {
        return <> {
            <Grid item xs={12}>
                <Grid container>
                    <Card className={CARD}>
                        <CardContent>
                            <Typography gutterBottom variant="h5" component="h2">
                                {TITLE}
                            </Typography>
                            <Typography variant="body2" component="p">
                                {PARA}
                            </Typography>
                        </CardContent>
                    </Card>
                </Grid>
            </Grid>
        } </>
    }
}

async function dataProcessing(parsedData: Papa.ParseResult<any>) {
    let count: number = 0;

    parsedData.data.forEach((lilArray) => {
        if (lilArray.length !== 0) {
            let dayObj = {
                id: count,
                day_name: "",
                day: 0,
                month: "",
                year: 0,
                morning: {},
                afternoon: {},
            };

            let dateSplit = lilArray[0].split("/");

            dayObj.day = parseInt(dateSplit[0]);
            dayObj.month = dateSplit[1]
            dayObj.year = parseInt(dateSplit[2]);

            if (lilArray.length == 3) {
                let op1 = lilArray[1].split(":")
                let cl1 = lilArray[2].split(":")

                if (parseInt(op1[0]) >= 12) {
                    dayObj.afternoon = { opening: { hour: op1[0], minute: op1[1] }, closure: { hour: cl1[0], minute: cl1[1] } }
                    dayObj.morning = null as any
                } else {
                    dayObj.morning = { opening: { hour: op1[0], minute: op1[1] }, closure: { hour: cl1[0], minute: cl1[1] } }
                    dayObj.afternoon = null as any
                }
            } else if (lilArray.length == 5) {
                let op1 = lilArray[1].split(":")
                let cl1 = lilArray[2].split(":")

                let op2 = lilArray[3].split(":")
                let cl2 = lilArray[4].split(":")

                dayObj.morning = { opening: { hour: op1[0], minute: op1[1] }, closure: { hour: cl1[0], minute: cl1[1] } }
                dayObj.afternoon = { opening: { hour: op2[0], minute: op2[1] }, closure: { hour: cl2[0], minute: cl2[1] } }
            } else {
                dayObj.morning = null as any
                dayObj.afternoon = null as any
            }

            SCHEDULE.push(dayObj);
            count++;
        }
    });
}

async function isBridgeOpen() {
    /*
    0 => No Schedule
    1 => Brigge is Open for walkers
    2 => Bridge is Close for walkers
    */
    var d = new Date()
    if (SCHEDULE.length != 0) {
        SCHEDULE.forEach(el => {
            if (OPEN_CODE != 1 && el.day == d.getDate() && el.month == (d.getMonth() + 1)) {
                let now_hour = d.getHours()
                let now_minute = d.getMinutes()
    
                if ((now_hour < el.morning?.opening.hour)
                    || (now_hour == el.morning?.closure.hour && now_minute > el.morning?.closure.minute
                        && now_hour < el.afternoon?.opening.hour)
                    || (now_hour > el.morning?.closure.hour
                        && now_hour < el.afternoon?.opening.hour)
                    || (now_hour == el.afternoon?.closure.hour && now_minute > el.afternoon?.closure.minute)
                    || (now_hour > el.afternoon?.closure.hour)) {
                    OPEN_CODE = 1
                }
            }
        });
        if (OPEN_CODE == 0) {
            OPEN_CODE = 2
        }
    }
    return
}

async function setAllText(){
    console.log(OPEN_CODE)
    if (OPEN_CODE == 0) {
        TITLE = "Impossible de récupérer les horaires";
        //card = classes.closeCard;
        CARD = "closeCard"
    } else if (OPEN_CODE == 1) {
        TITLE = "Le pont est accessible";
        PARA = "En tant que piéton ou cycliste vous allez pouvoir traverser le pont";
        CARD = "openCard"
        //card = classes.openCard;
    } else {
        TITLE = "Le pont est fermé";
        PARA = "En tant que piéton ou cycliste vous ne pouvez pas traverser le pont";
        CARD = "closeCard"
        //card = classes.closeCard;
    }
}

let exported = {
    WrapperMainCard
}

export default exported;