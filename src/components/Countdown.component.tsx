import useStyle from './Countdown.style';
import { FunctionalComponent, h } from 'preact';
import { useEffect, useState } from 'preact/hooks';

interface CountdownProps {
    date: number;
}

type TimeLeft = {
    hours: number;
    minutes: number;
    seconds: number;    
}

/**
 * Génère et affiche un décompte de temps qui se met à jour tout seul
 * @param props contient la date finale à atteindre
 * @returns un composant Preact
 */
const CountdownComponent: FunctionalComponent<CountdownProps> = props => {

    const classes = useStyle();

    /**
     * Calcule le temps restant par heure, minutes et secondes
     * @returns objet TimeLeft
     */
    const calculateTimeLeft = () => {
        const difference = +new Date(props.date) - +new Date();
        let timeLeft = {};
    
        if (difference > 0) {
            timeLeft = {
                hours: Math.floor((difference / (1000 * 60 * 60)) % 24),
                minutes: Math.floor((difference / 1000 / 60) % 60),
                seconds: Math.floor((difference / 1000) % 60),
            };
        }
    
        return timeLeft as TimeLeft;
    };

    /**
     * Prend un nombre en paramètre et sépare les chiffres dans un array
     * @param num nombre pour lequel récupérer les chiffres
     * @returns array de chiffres
     */
    const getDigits = (num : number) => {
        var digits = [];
        while (num >= 10) {
            digits.unshift(num % 10);
            num = Math.floor(num / 10);
        }
        digits.unshift(num);

        if (digits.length === 1) digits.unshift(0);
        return digits;
    }

    const [timeLeft, setTimeLeft] = useState<TimeLeft>(calculateTimeLeft());

    /**
     * Le timeout permet de mettre à jour l'affichage chaque seconde
     */
    useEffect(() => {
        const timer = setTimeout(() => {
            setTimeLeft(calculateTimeLeft());
        }, 1000);

        return () => clearTimeout(timer);
    });

    const timerComponents: JSX.Element[] = [];

    // Génération de l'affichage JSX
    Object.keys(timeLeft).forEach((interval) => {
        let i2 = interval as (keyof typeof timeLeft);
        let digits: number[];

        if (!timeLeft[i2]) digits = [0, 0]
        else digits = getDigits(timeLeft[i2]);
      
        timerComponents.push(
            <span className={classes[i2]}>
                <span className={classes.digit}>{digits[0]}</span>
                <span className={classes.digit}>{digits[1]}</span>
            </span>
        );
    });
    
    return (
        <div className={classes.countdown}>
            {timerComponents.length ? timerComponents : <span>Fin du compteur.</span>}
        </div>
    );
}
  
export default CountdownComponent;