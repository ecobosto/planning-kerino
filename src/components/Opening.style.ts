import { createStyles, makeStyles } from "@material-ui/core/styles";

export default makeStyles((theme) => createStyles({
    
    item : {
        padding: 25,
        backgroundColor: 'rgba(200, 200, 200, 0.3)',
        margin: 20,
        position: 'relative',
        maxWidth: 600,
        width: '70%',
        color: theme.palette.primary.light,
        borderRadius: 5,
      }
}),
);
