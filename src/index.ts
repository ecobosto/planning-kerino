import './assets/styles/style.css';
import App from './components/App.component';

/**
 * Point d'entrée de l'application :
 * On importe l'application Preact
 */
export default App;
